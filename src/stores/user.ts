import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import userSevice from "@/services/user";
import { useMassageStore } from "./massage";
import type User from "@/types/User";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const massageStore = useMassageStore();
  const dialog = ref(false);
  const showDel = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ login: "", name: "", password: "" });
  let delId = 0;

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      // editedProduct.value = { name: "", price: 0 };
    }
  });

  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userSevice.getUsers();
      users.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userSevice.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userSevice.saveUser(editedUser.value);
      }
      dialog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถบันทึก User ได้");
    }
    loadingStore.isLoading = false;
  }
  //  เคลียร์ ช่อง แบบฟังก์ชัน
  // function clearProduct() {
  //   editedProduct.value = { name: "", price: 0 };
  // }

  async function deletedProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await userSevice.deleteUser(delId);
      await getUsers();
      showDel.value = false;
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถลบข้อมูล User ได้");
    }
    delId = 0;
    loadingStore.isLoading = false;
  }

  function editProduct(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }

  function deleteProduct(id: number) {
    delId = id;
    showDel.value = true;
  }
  return {
    users,
    getUsers,
    dialog,
    editedProduct: editedUser,
    saveProduct,
    editProduct,
    deleteProduct,
    deletedProduct,
    showDel,
  };
});
