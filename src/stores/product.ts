import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMassageStore } from "./massage";

export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const massageStore = useMassageStore();
  const dialog = ref(false);
  const showDel = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  let delId = 0;

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedProduct.value = { name: "", price: 0 };
    }
  });

  async function getProducts() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }
  async function saveProduct() {
    loadingStore.isLoading = true;
    try {
      if (editedProduct.value.id) {
        const res = await productService.updateProduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      await getProducts();
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถบันทึก Product ได้");
    }
    loadingStore.isLoading = false;
  }
  //  เคลียร์ ช่อง แบบฟังก์ชัน
  // function clearProduct() {
  //   editedProduct.value = { name: "", price: 0 };
  // }

  async function deletedProduct() {
    loadingStore.isLoading = true;
    try {
      const res = await productService.deleteProduct(delId);
      await getProducts();
      showDel.value = false;
    } catch (e) {
      console.log(e);
      massageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    delId = 0;
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  function deleteProduct(id: number) {
    delId = id;
    showDel.value = true;
  }
  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteProduct,
    deletedProduct,
    showDel,
  };
});
