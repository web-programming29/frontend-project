export default interface Product {
  id?: number;

  name: string;

  price: number;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;
}
