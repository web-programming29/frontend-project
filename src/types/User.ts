export default interface User {
  id?: number;

  login: string;

  name: string;

  password: string;

  createdDate?: Date;

  updatedDate?: Date;

  deletedDate?: Date;
}
